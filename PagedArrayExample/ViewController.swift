//
// ViewController.swift
//
// Modified by James Limmer on 2015-12-31.
// Originally Created by Alek Åström. (https://github.com/MrAlek)
//


import UIKit
import PagedArray
import Alamofire
import SwiftyJSON

let PreloadMargin = 20 /// How many rows "in front" should be loaded
let PageSize = 20 /// Paging size
var baseRecord: Int?

// Request variables
let ReqProtocol = "http"
let ReqHost = "localhost"
let ReqPort = "3000"
let ReqAddressCount = "/api/kwickies/count?where[answered]=1"
let ReqAddressData = "/api/kwickies?where[answered]=1&filter[limit]=\(PreloadMargin)"

// URL Strings: Count - should return the total number of rows required, Data - should return the Data to populate
let urlStringCount = ReqProtocol + "://" + ReqHost + ":" + ReqPort + ReqAddressCount
let urlStringData = ReqProtocol + "://" + ReqHost + ":" + ReqPort + ReqAddressData

let dataArray = []

class ViewController: UITableViewController {

  let cellIdentifier = "Cell"
  let operationQueue = NSOperationQueue()
  
  var pagedArray = PagedArray<String>(count: 0, pageSize: PageSize)

  var PALoaded = false // flag to tell whether PagedArray has loaded or not
  
  var dataLoadingOperations = [Int: NSOperation]()
  var shouldPreload = true

    // MARK: User actions
    
    @IBAction func clearDataPressed() {
        dataLoadingOperations.removeAll(keepCapacity: true)
        operationQueue.cancelAllOperations()
        self.pagedArray.removeAllPages()
        tableView.reloadData()
    }
    
    @IBAction func preLoadingSwitchChanged(sender: UISwitch) {
        shouldPreload = sender.on
    }
    
    // MARK: Private functions
    
    private func configureCell(cell: UITableViewCell, data: String?) {
      print(data)
        cell.textLabel!.text = data ?? ""
    }
    
    private func loadDataIfNeededForRow(row: Int) {
      if let currentPage:Int = self.pagedArray.pageNumberForIndex(row){
        print("currentPage = \(currentPage)")
        if needsLoadDataForPage(currentPage) {
          loadDataForPage(currentPage)
        }
        
        let preloadIndex = row+PreloadMargin
        if preloadIndex < self.pagedArray.endIndex && shouldPreload {
          if let preloadPage:Int = self.pagedArray.pageNumberForIndex(preloadIndex) {
            if preloadPage > currentPage && needsLoadDataForPage(preloadPage) {
              loadDataForPage(preloadPage)
            }
          }
        }
      }
    }
    
    private func needsLoadDataForPage(page: Int) -> Bool {
        return self.pagedArray.pages[page] == nil && dataLoadingOperations[page] == nil
    }
    
    private func loadDataForPage(page: Int) {
      if let indexes:Range<Int> = self.pagedArray.indexes(page) {
        // Create loading operation
        let operation = DataLoadingOperation(indexesToLoad: indexes) { [unowned self] indexes, data in
          
          // Set elements on paged array
          print("setElements: \(data), pageIndex: \(page)")
          self.pagedArray.setElements(data, pageIndex: page)
          
          // Reload cells
          if let indexPathsToReload = self.visibleIndexPathsForIndexes(indexes) {
            self.tableView.reloadRowsAtIndexPaths(indexPathsToReload, withRowAnimation: .Automatic)
          }
          
          // Cleanup
          self.dataLoadingOperations[page] = nil
        }
        
        // Add operation to queue and save it
        operationQueue.addOperation(operation)
        debugPrint(operationQueue)
        dataLoadingOperations[page] = operation
      }
    }
    
    private func visibleIndexPathsForIndexes(indexes: Range<Int>) -> [NSIndexPath]? {
        return tableView.indexPathsForVisibleRows?.filter { indexes.contains($0.row) }
    }
}

// MARK: Table view datasource
extension ViewController {
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if self.PALoaded == false {
        
        self.pagedArray.removeAllPages()
        
        Alamofire.request(.GET, urlStringCount, parameters: nil, encoding: .JSON, headers: nil)
          .responseJSON { response in
              if let result = response.result.value {
                let json = JSON(result)
                if let count = json["count"].int {
                
                  print("# Kwickies: \(count)") // Just so we know in debug window
                  self.pagedArray = PagedArray<String>(count: count, pageSize: PageSize)
                  
                  self.tableView.reloadData()
                  print("Data reloaded...")
                  
                }
            }
              if let error = response.result.error {
                debugPrint(error)
              }
        }
        self.PALoaded = true
        return self.pagedArray.count
        
      }
      
      if self.PALoaded == true {
        return self.pagedArray.count
      }
      return self.pagedArray.count
    }
  
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      loadDataIfNeededForRow(indexPath.row)

      let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)!
      configureCell(cell, data: self.pagedArray[indexPath.row])
      return cell
    }
}

/// Test operation that produces nonsense numbers as data
class DataLoadingOperation: NSBlockOperation {
  
  var dataArray = [String]()
  
  func doRequestResponse(indexesToLoad: Range<Int>, baseRecord: Int, completionHandler: (AnyObject?, NSError?) -> ()) {
    let recordString = String(baseRecord)
    let urlString:String = urlStringData + "&filter[skip]=" + recordString
    
    Alamofire.request(.GET, urlString, parameters: nil, encoding: .JSON, headers: nil)
    .validate()
    .responseJSON { response in
      if let res = response.result.value {
        
        debugPrint(res) // Show the result in console
        completionHandler(res, nil)
        
      } else if let err = response.result.error {
        
        completionHandler(nil, err)
        
      }
    }
  }
    init(indexesToLoad: Range<Int>, completion: (indexes: Range<Int>, data: [String]) -> Void) {
        super.init()
      
        print("Loading indexes: \(indexesToLoad)")
      
        addExecutionBlock {
          print("Execution...")
          // Do things
          
        }
      
        completionBlock = {
          print("Complete...")
          
          if let record = baseRecord {
            baseRecord = record + PageSize // this might not work for scrolling upwards.
            if let record = baseRecord {
              self.doRequestResponse(indexesToLoad, baseRecord: record) { result, error in
                if let err = error {
                  
                  debugPrint(err)
                  return
                  
                } else if let res = result {
                  
                  let json = JSON(res)
                  debugPrint(json)
                  
                  let data:[String] = indexesToLoad.map {
                    var pointer = Int($0)
                    if pointer >= PageSize { pointer = pointer - record }
                    
                    if let index = json[pointer]["id"].int {
                      return "ID: \(index), #: \($0)"
                    }
                    return ""
                  }
                  
                  debugPrint("dataArray: \(data)")
                  debugPrint("IndexesToLoad: \(indexesToLoad)")
                  
                  NSOperationQueue.mainQueue().addOperationWithBlock {
                    completion(indexes: indexesToLoad, data: data)
                  }
                }
              }
            }
          } else {
            baseRecord = 0
            if let record = baseRecord {
              self.doRequestResponse(indexesToLoad, baseRecord: record) { result, error in
                if let err = error {
                  
                  debugPrint(err)
                  return
                  
                } else if let res = result {
                  
                  let json = JSON(res)
                  debugPrint(json)
                  
                  let data:[String] = indexesToLoad.map {
                    var pointer = Int($0)
                    if pointer >= PageSize { pointer = pointer - record }
                    if let index = json[pointer]["id"].int {
                      return "ID: \(index), #: \($0)"
                    }
                    return ""
                  }
                  
                  debugPrint("dataArray: \(data)")
                  debugPrint("IndexesToLoad: \(indexesToLoad)")
                  
                  NSOperationQueue.mainQueue().addOperationWithBlock {
                    completion(indexes: indexesToLoad, data: data)
                  }
                }
              }
            }
          }
        }
      }
    
}
